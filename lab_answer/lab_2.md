1. Apakah perbedaan antara JSON dan XML?
- JSON adalah bahasa meta yang berorientasi pada data dan digunakan untuk 
merepresentasikan data terstruktur dalam format yang dapat dibaca manusia.
- XML adalah bahasa markup yang berorientasi pada dokumen dan digunakan untuk
menambahkan informasi dalam bentuk teks biasa

2. Apakah perbedaan antara HTML dan XML?
- HTML fokus pada mengatur format tampilan halaman website 
dan bagaimana menampilkan data 
- XML fokus pada menyimpan dan transfer data 
Keduanya adalah bahasa markup yang memiliki peran berbeda,
tetapi saling melengkapi